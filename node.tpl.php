<?php
	$day = format_date($node->created, 'custom', 'd');
	$month = format_date($node->created, 'custom', 'M');
?>

<div class="node<?php print ($sticky) ? " sticky" : ""; ?>">
	<?php if ($page == 0) { ?>
    <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
	<div class="date">
		<span class="month"><?php print $month ?></span>
		<span class="day"><?php print $day ?></span>
	</div>
		
	<?php } else { ?>
    	<?php print $picture ?>
	<?php }; ?>

	
	<div class="content">
		<?php print $content ?>
	</div>
	
	<?php if ($submitted) { ?>
		<span class="submitted">
			by <?php print theme('username', $node);?>
		</span>
	<?php } ?>
	<div class="clear"></div>

	<?php if ($links) { ?>
		<div class="info">
			<?php print $links ?>
		</div>
    <?php }; ?>
	
</div>