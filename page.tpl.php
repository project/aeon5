<?php
// $Id:
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <meta name="author" content="Chris Herberte http://www.xweb.com.au" />
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
  <div id="container">
    <div id="headerwraper">
      <div id="header">

        <div id="headerleft">
          <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="Home"><img src="<?php print $logo ?>" alt="Home" /></a><?php } ?>
          <?php if ($site_name) { ?><span id="sitename"><a href="<?php print $base_path ?>" title="Home"><?php print $site_name ?></a></span><?php } ?>
          <?php if ($site_slogan) { ?><span id="slogan"><?php print $site_slogan ?></span><?php } ?>
        </div>

        <div id="headerright">
          <?php if ($primary_links) { ?><span id="primarylinks"><?php print theme('links', $primary_links) ?></span><?php } ?>
        </div>

      </div>
    </div>

    <div id="main">
      <div id="sidebar">
		    <?php if ($sidebar_left) { ?><?php print $sidebar_left ?><?php } ?>
					<?php if ($sidebar_right) { ?><?php print $sidebar_right ?><?php } ?>
      </div>

      <div id="contentwraper">
        <div id="content">       
          <?php if ($show_messages) { print $messages; } ?>
          <h2 class="title"><?php print $title ?></h2>
          <?php print $breadcrumb; ?> 
          <?php print $help ?>
          <?php print $tabs ?>
          <?php print $content; ?> 
          <?php print $feed_icons ?>
        </div>
      </div>
      <div class="clear"></div>
    </div>

    <div id="footerwraper">
      <div id="footer">
        <?php print $footer_message ?>
        <?php if (isset($secondary_links)) { ?>
        <?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'seclink')) ?>
        <?php } ?>
        <span id="designby">
          <?php if ($site_name) { ?>&copy; 2006 <?php print $site_name ?> : <?php } ?>Design by <a href="http://www.xweb.com.au/">Chris Herberte</a>
        </span>
      </div>
    </div>
  </div>
  <div class="clear"><?php print $closure ?></div>

</body>
</html>